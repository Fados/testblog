const path = require('path')
const express = require('express');
const app = express()
const bodyParser = require('body-parser')
const port = 5000;
const articles = require('./routes/articlesRoutes');
const Bars = require("./models/model.js");
const mongoose = require('mongoose');
//MODULES
//MIDDLEWARE
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', './views');
app.set('view engine', 'pug');
//console.log('Server listening on port ' + port);
//module.exports = app;

app.get('/', async (req, res) =>{
    try{
        res.render('bars',{ message: await Bars.find().lean().exec()});
    }
    catch(e){
        console.log(e);
    }
});
app.get('/faq', async (req, res) =>{
    res.render('faq');
});
app.use(articles);


mongoose.connect('mongodb+srv://alumin:c135wk79@tmg.56cyz.mongodb.net/sandbox?retryWrites=true&w=majority',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    },
    (connection) => {
        try {
            app.listen(port, () => {
                console.log('server started on port', port);
            });
        } catch (err) {
            throw err;
        }
    }
)