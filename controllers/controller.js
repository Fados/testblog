const Bars = require("../models/model.js");
const {checkSchema} = require("../utils/validator.js");
module.exports = {
    postPost:async (req, res) => {
        try{
            const text = req.body.text;

                await checkSchema.validateAsync(req.body);

                await Bars.create({
                    text: text,
                })
                return res.send({status:'OK'})
        }catch(e){
            return res.send({err:e.message});
        }


    },
    getPosts:async(req, res) => {
        try {
            const bar = await Bars.find();
            return res.send({bar: bar});
        } catch (e) {
            return res.send({e: e});
        }

    },

    deletePost:async (req, res) => {
        const id = req.params.id;
        try {
            await Bars.findOneAndDelete({_id: id});
            return res.send('ok')
        } catch (e) {
            return res.json(e);
        }
    },
    likePosst:async(req, res) => {
        try {
            await Bars.findOneAndUpdate({_id: req.params.id}, {'$inc': {'likes': 1}}, {
                new: true
            });
            return res.send('ok');
        } catch (e) {
            return res.json(e);
        }
    },
    rewritePost:async(req, res) => {
        try {
            await Bars.findOneAndUpdate({_id: req.params.id}, {
                '$set': {
                    'text': req.body.text
                }

            }, {
                new: true
            });
            return res.send('ok');
        } catch (e) {
            return res.json(e);
        }
    }
}

