const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//DATABASE

const blogSchema = new Schema({
    date:{
        type:Date,
        default:Date.now
    },
    text:{
        type:String
    },
    likes:{
        type:Number,
        default:0
    }
})
const PostModel = mongoose.model('dim_bars',blogSchema);
module.exports=PostModel;