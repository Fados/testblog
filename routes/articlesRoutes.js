const express = require('express');
const ArticlesController = require("../controllers/controller");
const router = new express.Router();
    router.post('/create',ArticlesController.postPost);
    router.get('/user_get',ArticlesController.getPosts);
    router.get('/post_delete/:id',ArticlesController.deletePost);
    router.post('/like/:id',ArticlesController.likePosst);
    router.post('/rewrite/:id',ArticlesController.rewritePost);
module.exports = router;